## Mechanics for uploading patients data


### Note 1: 

I am not allowing the identifier to be 0, so you need to modify the sample.csv dataset and change the id of the first patient before submitting.


### Note 2:

For security purposes I am not returning the identifier (primary key) in the json return, I created a public id so that a specific patient can be retrieved.


### Instructions:


### Create the virtual environment (virtualenv) and run the command:

```pip install -r requirements.txt```


### Run the migrations using the command:

```python manage.py migrate```


### Run the tests using the command:

```python manage.py test apps.patient.tests```


### Run the system using the command:

```python manage.py runserver```


### GET
http://localhost:8000/api/patients/


### GET
http://localhost:8000/api/patients/{public_id}


### GET
http://localhost:8000/api/patients/?classification={classification}


### POST (Multipart Form CSV, name=patients)
http://localhost:8000/api/upload_patients/


### POST
http://localhost:8000/api/patients/
