from rest_framework.routers import DefaultRouter
from .views import PatientViewSet, UploadPatientViewSet


router_patient = DefaultRouter()
router_patient.register(r'upload_patients', UploadPatientViewSet, basename='upload_patients')
router_patient.register(r'patients', PatientViewSet, basename='patients')