import io
from rest_framework import status
from rest_framework.test import APITestCase


PATIENTS = '/api/patients/'
UPLOAD_PATIENTS = '/api/upload_patients/'
PATIENT_NOT_FOUND = '/api/patients/2545798'


class PatientTestCase(APITestCase):

    def test_patients_post(self):
        file_name = 'filetest.csv'
        with open('files/filetest.csv', 'rb') as input_file:            
            input_file_stream = io.BytesIO(input_file.read())
        data = {            
            'patients': (input_file_stream, file_name),            
            'file': file_name
        }

        response = self.client.post(UPLOAD_PATIENTS, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)


    def test_patients_post_error(self):
        file_name = 'filetest.csv'
        with open('files/filetest.csv', 'rb') as input_file:            
            input_file_stream = io.BytesIO(input_file.read())
        data = {            
            'patients': (input_file_stream, file_name),            
            'file': file_name
        }

        response = self.client.post(UPLOAD_PATIENTS, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


    def test_get_all_patients(self):
        response = self.client.get(PATIENTS)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_get_all_patients_by_classification(self):
        response = self.client.get(f'{PATIENTS}?classification=ckd', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_get_single_patient(self):
        self.test_patients_post()
        response = self.client.get(PATIENTS, format='json')
        if response.json()['results'][0]:
            public_id = response.json()['results'][0]['public_id']
            response = self.client.get(f'{PATIENTS}?public_id={public_id}', format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)


    def test_get_single_patient_not_found(self):
        response = self.client.get(PATIENT_NOT_FOUND, {}, True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
