from django_filters import rest_framework as filters
from .models import Patient


lookup_types_patient = {
    'public_id': ['exact', 'in', ],
    'classification': ['exact', 'iexact', 'icontains', 'istartswith', 'iendswith', 'in', 'iregex']
}


class PatientFilter(filters.FilterSet):
    class Meta:
        model = Patient
        fields = {
            'public_id': lookup_types_patient['public_id'],
            'classification': lookup_types_patient['classification']
        }
