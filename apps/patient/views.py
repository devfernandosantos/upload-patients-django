import numpy as np
import pandas as pd
from django.db import transaction
from django.utils.datastructures import MultiValueDictKeyError
from rest_framework import viewsets
from rest_framework.response import Response

from .filters import PatientFilter
from .models import Patient
from .serializers import PatientSerializer, PatientSerializerRetrieve


class PatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializerRetrieve
    lookup_field = 'public_id'
    filterset_class = PatientFilter


class UploadPatientViewSet(viewsets.ModelViewSet):
    queryset = Patient.objects.all()
    serializer_class = PatientSerializer
    http_method_names = ['post']

    @transaction.atomic
    def create(self, request, *args, **kwargs):

        sid = transaction.savepoint()

        try:
            df = self.handle_dataset()
        except MultiValueDictKeyError:
            return Response({'msg': 'Content-Type (Multipart Form) [patients] [file.csv]'}, status=400)

        patients = self.dataframe_to_object(df['patients'], df['keys'])

        try:
            for patient in patients:

                if int(patient['id']) <= 0:
                    transaction.savepoint_rollback(sid=sid)
                    return Response({'msg': f'Patient id cannot be {patient["id"]}'}, status=400)

                if Patient.objects.filter(id=patient['id']):
                    transaction.savepoint_rollback(sid=sid)
                    return Response({'msg': f'Integrity Error, patient {patient["id"]} is already in the database'}, status=400)

                serializer = PatientSerializer(data=patient)

                if serializer.is_valid():
                    Patient.objects.create(**patient)
                else:
                    transaction.savepoint_rollback(sid=sid)
                    return Response({'msg': f'{str(serializer.errors)} Patient id {patient["id"]}'}, status=400)

        except:
            transaction.savepoint_rollback(sid=sid)
            return Response({'msg': 'Unexpected error'}, status=500)

        return Response({'msg': 'Successful upload'}, status=201)

    def handle_dataset(self):
        df = pd.read_csv(self.request.FILES['patients'])

        df.drop(df.columns[26:], axis=1, inplace=True)
        df.replace(np.nan, None, regex=True, inplace=True)
        df.replace('[^a-zA-Z0-9]', None, regex=True, inplace=True)

        float_datatype = ['age' 'bp' 'sg' 'al' 'su' 'bgr' 'bu' 'sc' 'sod' 'pot' 'hemo', 'rc']
        int_datatype = ['pcv', 'wc']
        object_datatype = ['htn' 'dm' 'cad' 'appet' 'pe' 'ane' 'rbc' 'pc' 'pcc' 'ba']

        for column in df.columns:
            if column in float_datatype or column in int_datatype:
                df[column].fillna(0, inplace=True)
            elif column in object_datatype:
                df[column].fillna(None, inplace=True)

        df = {
            'patients': df.values,
            'keys': df.columns
        }

        return df

    def create_object(self, key_list, values):
        patient_object = {}

        for index, value in enumerate(key_list):
            patient_object.update({value: values[index]})

        return patient_object

    def dataframe_to_object(self, data, keys):
        patients_list = np.array([])

        for patient in data:
            patients_list = np.append(patients_list, self.create_object(keys, patient))

        return patients_list
