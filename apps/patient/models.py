from django.db import models
import uuid


class Patient(models.Model):
    
    public_id = models.UUIDField(
        default=uuid.uuid4
    )

    age = models.FloatField(
        null=True, blank=True
    )

    bp = models.FloatField(
        null=True, blank=True
    )

    sg = models.FloatField(
        null=True, blank=True
    )

    al = models.FloatField(
        null=True, blank=True
    )

    su = models.FloatField(
        null=True, blank=True
    )

    rbc = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    pc = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    pcc = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    ba = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    bgr = models.FloatField(
        null=True, blank=True
    )

    bu = models.FloatField(
        null=True, blank=True
    )

    sc = models.FloatField(
        null=True, blank=True
    )

    sod = models.FloatField(
        null=True, blank=True
    )

    pot = models.FloatField(
        null=True, blank=True
    )

    hemo = models.FloatField(
        null=True, blank=True
    )

    pcv = models.IntegerField(
        null=True, blank=True
    )

    wc = models.IntegerField(
        null=True, blank=True
    )

    rc = models.FloatField(
        null=True, blank=True
    )

    htn = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    dm = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    cad = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    appet = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    pe = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    ane = models.CharField(
        max_length=100,
        null=True, blank=True
    )

    classification = models.CharField(
        max_length=100,
    )

    class Meta:
        ordering = ['-id']
        db_table = 'patient'
        verbose_name = 'Patient'
        verbose_name_plural = 'Patients'
