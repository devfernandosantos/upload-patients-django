from django.urls import path, include
from apps.patient.urls import router_patient


base_url_v1 = 'api/'

urlpatterns = [
    path(base_url_v1, include(router_patient.urls), name='patients'),
]
